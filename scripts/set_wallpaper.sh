#!/bin/sh
DIR="$HOME/.wallpapers/"
FILE="$(find $DIR -type f | shuf -n1)"
xwallpaper --center "$FILE"