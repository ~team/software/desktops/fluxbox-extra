#!/bin/sh

#  generic setup script for desktop (fluxbox)
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 2 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program. If not, see <http://www.gnu.org/licenses/>.
#

global_packagename="fluxbox-extra"

global_script_dialogbacktitle="Setup for Desktop (Fluxbox)"

global_script_screenresolutions=(
	1024x768
	1280x720
	1280x800
	1360x768
	1366x768
	1440x900
	1600x900
	1680x1050
	1920x1080
	1920x1200
)

global_script_usergroups=(
	audio
	video
	optical
	sys
	storage
	power
	input
	network
	games
	wheel
)

die() {
	printf 'Error: %s\n' "${@}" 1>&2
	exit 1
}

if [ "$(id -u)" != 0 ]; then
	die "Please run this script with root-privileges."
fi

local_username=$(dialog --no-tags --no-cancel --clear --title "Enter name of local user" \
			--backtitle "$global_script_dialogbacktitle" \
			--inputbox "" 8 40 2>&1 > /dev/tty)
clear

if [ -z "$local_username" ]; then
	die "The provided username was empty, setup aborted for now. You will need to run it manual again."
fi

if ! id "$local_username" >/dev/null 2>&1; then
	die "The provided user was not found, setup aborted for now. You will need to run it manual again."
fi

local_screenresolution=$(dialog --no-tags --no-cancel --clear --backtitle "$global_script_dialogbacktitle" \
				--radiolist "Choose your concurrent screen-resolution used for the desktop-wallpaper:" 15 40 30 \
				0 "1024x768" on \
				1 "1280x720" off \
				2 "1280x800" off \
				3 "1360x768" off \
				4 "1366x768" off \
				5 "1440x900" off \
				6 "1600x900" off \
				7 "1680x1050" off \
				8 "1920x1080" off \
				9 "1920x1200" off 2>&1 > /dev/tty)
clear
local_wallpaper_filename=wallpaper_${global_script_screenresolutions[$local_screenresolution]}.png

cp /usr/share/${global_packagename}/configurations/.xinitrc /home/${local_username}/
chmod +x /home/${local_username}/.xinitrc
chown ${local_username}:users /home/${local_username}/.xinitrc
mkdir -p /home/${local_username}/.fluxbox
mkdir -p /home/${local_username}/.icons
mkdir -p /home/${local_username}/.launchers
mkdir -p /home/${local_username}/.config/jgmenu
mkdir -p /home/${local_username}/.config/tint3
mkdir -p /home/${local_username}/.wallpapers
mkdir -p /home/${local_username}/scripts_run
cp /usr/share/${global_packagename}/configurations/fluxbox/* /home/${local_username}/.fluxbox/
sed -i -e "s/\%USERHOME\%/\/home\/$local_username/g" /home/${local_username}/.fluxbox/init
cp /usr/share/${global_packagename}/configurations/jgmenu/* /home/${local_username}/.config/jgmenu/
cp /usr/share/${global_packagename}/configurations/tint3/* /home/${local_username}/.config/tint3/
cp /usr/share/${global_packagename}/icons/* /home/${local_username}/.icons/
cp /usr/share/${global_packagename}/launchers/* /home/${local_username}/.launchers/
sed -i -e "s/\%USERHOME\%/\/home\/$local_username/g" /home/${local_username}/.launchers/menu.desktop
cp /usr/share/${global_packagename}/scripts/* /home/${local_username}/scripts_run/
chmod +x /home/${local_username}/scripts_run/*
cp /usr/share/${global_packagename}/wallpaper/${local_wallpaper_filename} /home/${local_username}/.wallpapers/
chown -R ${local_username}:users /home/${local_username}/.fluxbox/
chown -R ${local_username}:users /home/${local_username}/.config/jgmenu/
chown -R ${local_username}:users /home/${local_username}/.config/tint3/
chown -R ${local_username}:users /home/${local_username}/.icons/
chown -R ${local_username}:users /home/${local_username}/.launchers/
chown -R ${local_username}:users /home/${local_username}/.wallpapers/
chown -R ${local_username}:users /home/${local_username}/scripts_run/
